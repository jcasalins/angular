import { Component, OnInit } from '@angular/core';
import { Coches } from './coches';
import { PeticionesService } from '../services/peticiones.service';

@Component({
  selector: 'app-coches',
  templateUrl: './coches.component.html',
  styleUrls: ['./coches.component.scss'],
  providers: [PeticionesService]
})
export class CochesComponent implements OnInit {

  public coche:Coches;
  public coches:Array<Coches>;
  public articulos;
  constructor(
    private _peticionesService: PeticionesService
  ) { 
    this.coche = new Coches('', '', '');
    this.coches = [
      new Coches('Seat panda', '120', 'blanco'),
      new Coches('Renault Clio', '110', 'Azul')

    ]
   }
    ngOnInit() {
      this._peticionesService.getArticulos().subscribe(
        result =>{
          this.articulos =  result
          if (!this.articulos) {
            console.log('ERROR en el servidor');
            
          }
         // console.log(result);
          
        },
        error => {
          let errorM = <any>error;
          console.log(errorM);
          
        }
      )
    }
   onSubmit(){
     this.coches.push(this.coche);
     this.coche = new Coches('', '', '');
     
   }
 

}

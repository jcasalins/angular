import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fruta',
  templateUrl: './fruta.component.html',
  styleUrls: ['./fruta.component.scss']
})
export class FrutaComponent implements OnInit {
  public nombre_componente = 'Componente de fruta';
  public listado_frutas = 'Naranja, Manzana, Piña y sandia';

  public nombre:string;
  public edad:number;
  public mayorDeEdad:boolean;
  public trabajos:Array<any> = ['Carpintero', 44 , 'Fontanero'];
  
  comodin:any;


  constructor() {
      
    this.nombre = 'Jairo Casalins';
    this.edad = 31;
    this.mayorDeEdad=false;
    this.comodin= 'Si';
      
  }

  ngOnInit() {
  }

}

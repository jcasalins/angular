import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RopaService {
  public nombre_prenda= 'Pantalones Vaqueros';
  public coleccion_ropa =['pantalones blancos', 'camiseta roja']
  constructor() { }
  prueba(nombre_prenda){
    return nombre_prenda;
  }

  addRopa(nombre_prenda:string):Array<string>{
    this.coleccion_ropa.push(nombre_prenda)
    return this.getRopa();
  }

  deleteRopa(i:number){
    this.coleccion_ropa.splice(i, 1);
    return this.getRopa();
  }
  getRopa():Array<string>{
    return this.coleccion_ropa;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/observable'

@Injectable({
  providedIn: 'root'
})
export class PeticionesService {


  public url: string;


  constructor( private _http:HttpClient) { 
    this.url = 'https://jsonplaceholder.typicode.com/posts';
  }

  getPrueba(){
    return 'hola mundo de prueba';
  }

  getArticulos():Observable<any>{

    return this._http.get(this.url)
  }


}

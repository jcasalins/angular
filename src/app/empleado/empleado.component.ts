import { Component, OnInit } from '@angular/core';
import { Empleado } from './empleado';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {

  public title = 'Componente Empleados';

  public empleado:Empleado;
  public trabajadores:Array<Empleado>;
  public trabajador_externo:boolean;
  public color:string;
  public color_seleccionado:string;
  

  constructor() { 
    this.empleado = new Empleado('Jairo Casalins', 45, 'Casinero', true);
    this.trabajadores= [
      new Empleado('Jairo Casalinss', 45, 'Casinero', true),
      new Empleado('Jairo Casalinsss', 45, 'Casinero', true),
      new Empleado('Jairo Casalinssss', 45, 'Casinero', true),
    ]
    this.trabajador_externo = true;
    this.color = 'red';
    this.color_seleccionado= '#ccc'
  }

  ngOnInit() {
    
  }

  cambiarExterno(valor) {
    this.trabajador_externo = valor;
  }
  logColorSeleccionado(){
    console.log(this.color_seleccionado);
    
  }
}

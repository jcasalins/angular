export class Empleado {
    constructor (
        public name:string,
        public age:number,
        public position:string,
        public hired:boolean        
    ){}
}

import { Component, OnInit } from '@angular/core';
import { RopaService } from '../services/ropa.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [RopaService]
})
export class HomeComponent implements OnInit {
  public title = 'Pagina Principal';
  public listado_ropa:Array<string>;
  public prenda_a_guardar:string;
  public nombre = 'JAIRO casalins Blanco'

  public fecha;
  constructor(
    private _ropaService: RopaService
  ) {
    this.fecha = new Date();
  }

  ngOnInit() {
    this.listado_ropa =  this._ropaService.getRopa();
    console.log(this._ropaService.prueba('nike'));
  }

  guardar_prenda(){
    this._ropaService.addRopa(this.prenda_a_guardar);
    this.prenda_a_guardar = null;
  }
  eliminarPrenda(index:number){
    this._ropaService.deleteRopa(index);
  }
}
